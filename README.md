![Common Crawl Logo](http://commoncrawl.org/wp-content/uploads/2012/04/ccLogo.png)

# Common Crawl WARC Examples

This repository supports the **University of the Pacific** Spring 2016
[Common Crawl Tutorial](http://ecs-network.serv.pacific.edu/ecpe-276/projects/commoncrawl-tutorial),
and is derived from the [cc-warc-examples](https://github.com/commoncrawl/cc-warc-examples) repository.

This repository contains three examples for processing WARC files in Hadoop MapReduce jobs:

+ [WARC files] HTML tag frequency counter using raw HTTP responses (WARCTagCounter.java)
+ [WAT files] Server response analysis using response metadata (WATServerType.java)
+ [WET files] Classic word count example using extracted text (WETWordCount.java)

All three assume that the files are stored in Common Crawl's Amazon S3 bucket.

Tested with Hadoop 2.6.0 / Amazon EMR 4.2.0


# License

MIT License, as per `LICENSE`
